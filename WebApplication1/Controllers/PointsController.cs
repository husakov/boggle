using System;
using System.Collections.Generic;
using WebApplication1.Models;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class PointsController : Controller
    {
        // GET
        public int WordListScore(string[] list)
        {
            int score = 0;
            for (int i = 0; i < list.Length; i++)
            {
                if (list[i].Length < 4 && list[i].Length > 2)
                {
                    score = score + 1;
                }
                if (list[i].Length == 5)
                {
                    score = score + 2;
                }
                if (list[i].Length == 6)
                {
                    score = score + 3;
                }
                if (list[i].Length == 7)
                {
                    score = score + 6;
                }
                if (list[i].Length > 8)
                {
                    score = score + 11;
                }
            }
            return score;
        }
        public string MultiPlayerScore()
        {    
            var players = new List<Player>{ 
                new Player() { PlayerName = "Samir1", List = new []{ "Maheshhand", "Mikeold", "Rajeniwal", "Praveenumar", "Dineseniwal" } },
                new Player() { PlayerName = "Samir2", List = new []{ "Maheshhand", "Mikeold", "Rajeniwal", "Praveenumar", "Dineseniwal" } },
                new Player() { PlayerName = "Samir3", List = new []{ "Maheshfdshand", "Mikeold", "Rajeniwal", "Praveenumar", "Dineseniwal" } },
                new Player() { PlayerName = "Samir4", List = new []{ "Maheshhand", "Mikeold", "Rajeniwal", "Praveenumar", "Dineseniwal" } },
                new Player() { PlayerName = "Samir5", List = new []{ "Mahefsdfdsfshhand", "old", "Rajeniwal", "Praveenumar", "Dineseniwal" } },
            };
            string[] scoreList = new string[players.Count];
            foreach (var player in players)
            {
                int i = 0;
                scoreList[i]=player.PlayerName + " score: " + WordListScore(player.List);
                i++;
            }
            Console.WriteLine(String.Join(Environment.NewLine, scoreList));
            return String.Join(Environment.NewLine, scoreList);
        }
    }
}